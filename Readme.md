# Webpack bootstrap boilerplate

[![build status](https://gitlab.com/GomatoX1/webpack-boilerplate/badges/master/build.svg)](https://gitlab.com/GomatoX1/webpack-boilerplate/commits/master)

### Description
Webpack based front-end development boilerplate which comes with these futures:
* sass
* sass-lint
* autoprefixer
* jshint
* svg font generator
* yarn compatability
* browser-sync
* nodemon
* ejs
* babel

### Commands with npm
* **npm start** - starts webpack with nodemon, webpack watch and browser-sync over proxy, default proxy is http://localhost:80.  
* **npm run serve** - starts webpack-dev-server, nodemon, webpack watch and browser-sync over proxy default proxy is http://localhost:80.  
* **npm run production** - runs webpack production task and adds build results to app/assets/dist folder.  

### Commands with yarn
* **yarn run start** - same as *npm start*  
* **yarn run serve** - same as *npm run serve*  
* **yarn run production** -- same as *npm run production*  

### Start, run serve and run production scripts information
Then using `npm start` or `npm run production` there are two files `index.ejs` (development) and `index.production.ejs` (production), then you runing production your app wil automaticaly include script and css and generetate it into index.html file.

### Configurations
All project based configurations should be configured in `webpack.config.project.js` file.  
Postcss and browser support configurations are located in `postcss.config.js`.  
Default editor configurations are located in `.editorconfig`

#### Font-icon
After setting up the plugin your css has now a new feature:  
font-icon declarations  

    a:before {
        font-icon: url('./icon.svg');
        transition: 0.5s color;
    }

    a:hover:before {
        color: red;
    }

More information you can find here: https://www.npmjs.com/package/iconfont-webpack-plugin  
Error: `OTS parsing error: head: Bad ppm of 8` - you have to prepare all svg the same height to avoid this error.
