const path = require('path');
const webpack = require('webpack');
const StyleLintPlugin = require('stylelint-webpack-plugin');
const IconfontWebpackPlugin = require('iconfont-webpack-plugin');
const ExtractTextPlugin = require("extract-text-webpack-plugin");
const HtmlWebpackPlugin = require('html-webpack-plugin');
const projectSettings = require( './webpack.config.project.js' );

const paths = projectSettings.paths;
const libraries = projectSettings.libraries;
const alias = projectSettings.alias;

module.exports = {
    entry: projectSettings.entry,
    output: {
        filename: projectSettings.outputFilename,
        path: paths.dist
    },
    resolve: {
        alias: alias
    },
    cache: true,
    module: {
        rules: [
            {
                test: /\.scss$/,
                use: ExtractTextPlugin.extract({
                    fallback: "style-loader",
                    use: [
                        "css-loader",
                        "postcss-loader",
                        "sass-loader"
                    ]
                })
            },
            {
                test: /\.html$/,
                loader: 'html-loader'
            },
            {
                test: /\.js$/, // include .js files
                exclude: /node_modules|vendors/, // exclude any and all files in the node_modules folder
                use: [
                    {
                        loader: "jshint-loader",
                        options: {}
                    }
                ]
            },
            {
                test: /\.ejs$/,
                loader: 'ejs-compiled-loader'
            },
            {
                test: /\.js$/,
                exclude: /(node_modules|bower_components)/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['env']
                    }
                }
            }
        ]
    },
    plugins: [
        new StyleLintPlugin({
            configFile: '.stylelintrc',
            files: path.join(paths.assetsRelative, "sass/**/*.s?(a|c)ss")
        }),
        new HtmlWebpackPlugin({
            filename: '../../index.html',
            template: 'app/index.production.ejs'
        }),
        new IconfontWebpackPlugin(),
        new webpack.optimize.UglifyJsPlugin(),
        new ExtractTextPlugin(projectSettings.cssFileName),
        new webpack.DefinePlugin({
            'process.env': {
                NODE_ENV: '"production"'
            }
        })
    ]
};
