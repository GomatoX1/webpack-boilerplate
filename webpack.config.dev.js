const path = require('path');
const webpack = require('webpack');
const BrowserSyncPlugin = require('browser-sync-webpack-plugin');
const StyleLintPlugin = require('stylelint-webpack-plugin');
const IconfontWebpackPlugin = require('iconfont-webpack-plugin');
const WebpackNotifierPlugin = require('webpack-notifier');
const projectSettings = require( './webpack.config.project.js' );
const HtmlWebpackPlugin = require('html-webpack-plugin');

const devServer = process.argv[1].indexOf( 'webpack-dev-server' ) > -1;
const paths = projectSettings.paths;
const libraries = projectSettings.libraries;
const alias = projectSettings.alias;

var setup = {
    entry: projectSettings.entry,
    output: {
        filename: projectSettings.outputFilename,
        path: paths.dist
    },
    resolve: {
        modules: [__dirname, 'node_modules'],
        extensions: ['*','.js','.jsx'],
        alias: alias,
    },
    cache: true,
    module: {
        rules: [
            {
                test: /\.scss$/,
                use: [
                    {
                        loader: "style-loader" // creates style nodes from JS strings
                    },
                    {
                        loader: "css-loader", // translates CSS into CommonJS,
                        options: {
                            importLoaders: 1,
                            sourceMap: projectSettings.sourceMaps
                        }
                    },
                    {
                        loader: "postcss-loader", // compiles Sass to CSS
                        options: {
                             sourceMap: projectSettings.sourceMaps
                        }
                    },
                    {
                        loader: "sass-loader", // compiles Sass to CSS
                        options: {
                             sourceMap: projectSettings.sourceMaps
                        }
                    },
                ]
            },
            {
                test: /\.html$/,
                loader: 'html-loader'
            },
            {
                test: /\.js$/, // include .js files
                exclude: /node_modules|vendors/, // exclude any and all files in the node_modules folder
                use: [
                    {
                        loader: "jshint-loader",
                        options: {}
                    }
                ]
            },
            {
                test: /\.ejs$/,
                loader: 'ejs-compiled-loader'
            },
            {
                test: /\.js$/,
                exclude: /(node_modules|bower_components)/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['env']
                    }
                }
            }
        ]
    },
    plugins: [
        new BrowserSyncPlugin({
            host: projectSettings.browserSync.host,
            port: projectSettings.browserSync.port,
            open: true,
            proxy: devServer ? 'http://localhost:9000' : projectSettings.browserSync.proxy // proxy to server
        },{
            reload: !devServer
        }),
        new WebpackNotifierPlugin({
            title: 'Webpack boilerplate',
            timeout: 3,
            sound: true,
            wait: false
        }),
        new StyleLintPlugin({
            configFile: '.stylelintrc',
            failOnError: false,
            files: path.join(paths.assetsRelative, "sass/**/*.s?(a|c)ss")
        }),
        new IconfontWebpackPlugin(),
        new HtmlWebpackPlugin({
            template: 'app/index.ejs'
        }),
        new webpack.ProvidePlugin(libraries)
    ],
    // server configurations
    devServer: {
        contentBase: path.join(__dirname, "app"),
        compress: false,
        port: 9000,
        hot: true,
        overlay: {
            errors: true
        },
        clientLogLevel: "error"
    }
};

if ( devServer ) {

    setup.plugins.push( new webpack.HotModuleReplacementPlugin() );
}

module.exports = setup;
