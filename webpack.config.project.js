/**
 * Here is default libraries and dependencies setup for project
 * Also if you going to remove some livraries don't  forget to remove and from bower.json
 *
 * !!! After changing this file you have to restart server
 *
 */

const path = require('path');

module.exports = {
    entry: [
        path.join(__dirname, 'app/index.js' )
    ],
    sourceMaps: false,
    outputFilename: 'bundle.js',
    browserSync: {
        host: 'localhost',
        port: 3000,
        proxy: 'http://localhost:80/'
    },
    cssFileName: 'app.css',
    alias: {},
    paths: {
        app: path.join( __dirname, "./app/"),
        assets: path.join( __dirname, "./app/assets/"),
        // need for some loaders
        assetsRelative: "./app/assets/",
        vendors: path.join( __dirname, "./app/assets/vendors/"),
        dist: path.join( __dirname, "./app/assets/dist/")
    },
    libraries: {}
};
